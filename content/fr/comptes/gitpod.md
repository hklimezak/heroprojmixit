---
title: "2/2 - Créer un compte Gitpod avec votre compte GitLab"
date: 1042-04-01T00:00:00+01:00
tags: []
featured_image: ""
description: ""
---
__*(si vous n’en avez pas)*__

 
Vous allez forker un projet existant. Ce projet vous servira pour créer le compte GitPod et vous amusez avec si vous le désirez.

Pour cela rendez-vous sur le projet suivant : https://gitlab.com/gitops-heros/requirements


![Inception : fork du projet que vous êtes en train de lire](../images/02-fork-project.png)

Cliquez sur le lien de `Fork` en haut à droite.  
Sur la nouvelle page, choisissez le nom de “l’organisation” (votre identifiant gitlab si vous venez de créer le compte), si vous le désirez changer le nom du projet  
Puis validez en cliquant sur `Fork Project`.  

Vous allez maintenant ouvrir ce projet dans Gitpod, pour cela, vous avez une combobox au centre de l’écran indiquant `Web IDE`  
![](../images/03-change-web-ide.png)

Changez `Web IDE` par `Gitpod`  

![](../images/04-to-gitpod.png)

Puis cliquez sur `Gitpod`  

Il vous sera demandé d’activer GitPod en cliquant sur `Enable Gitpod`  

![](../images/05-enabled-gitpod.png)

Une fois, cela fait, vous pouvez de nouveau cliquer sur `Gitpod`  
Vous arrivez sur Gitpod, une avant avant dernière validation est demandée, il faut cliquer sur `Continue with GitLab`  


![](../images/06-link-gitlab-account.png)

Vous y êtes presque, il vous est demandé d'autoriser Gitpod à accéder à votre compte Gitlab (nécessaire pour accéder à vos projets Gitlab). Cliquez sur `Authorize`  


![](../images/07-autorize-access-to-gitlab.png)

Validez-en cliquant sur Continue et vous êtes enfin sur Gitpod, votre compte est presque validé, après un dernier choix et des dernières questions et un dernier contrôle votre compte est up & running et vous êtes connecté à votre projet.  


![](../images/08-welcome-to-gitpod.png)

Vous avez fait le plus pénible 🥳  
Vous pouvez maintenant fermer le workspace Gitpod, en cliquant en bas à gauche sur “Gitpod” puis choisissez “Stop workspace”.  
__Vous pouvez utiliser Gitpod dans la limite de votre crédit gratuit de 50h par mois.__
